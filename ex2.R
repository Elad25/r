titanic<-read.csv('train.csv')
titanic
View(titanic)

removecolumns<- titanic[,-c(1,4,9,10,11)] # remove columns 4,9 . we keeping column 1 for arrange it later
View(removecolumns)

sibspfactor <- removecolumns$SibSp #sibSp column into vector
sibspfactor
sibspfactor <- factor(sibspfactor) # factor function get vector and present us the vector levels
sibspfactor
data = factor(sibspfactor, labels=c("zero", "one", "two", "three", "four", "five", "eight")) #set labels instead of levels numbers
data


Parchfactor <- removecolumns$Parch #Parch column into vector
Parchfactor
Parchfactor <- factor(Parchfactor) # factor function get vector and present us the vector levels
Parchfactor
data = factor(Parchfactor, labels=c("zero", "one", "two", "three", "four", "five", "six")) #set labels instead of levels numbers
data

data = factor(sibspfactor, labels=c("zero", "one", "two", "three", "four", "five", "eight")) #set labels instead of levels numbers
data
removecolumns$SibSp <- data #repleace sibSp column


data = factor(Parchfactor, labels=c("zero", "one", "two", "three", "four", "five", "six")) #set labels instead of levels numbers
data
removecolumns$Parch <- data #repleace Parch column
View(removecolumns)

# full example of factor
Survivedfactor <- removecolumns$Survived #Survived column into vector
Survivedfactor
Survivedfactor <- factor(Survivedfactor) # factor function get vector and present us the vector levels
Survivedfactor
SurvivedData = factor(Survivedfactor, labels=c("Not Survived", "Survived")) #set labels instead of levels numbers
SurvivedData
removecolumns$Survived <- SurvivedData #repleace Survived column
View(removecolumns)


Pclassfactor <- removecolumns$Pclass #Pclass column into vector
Pclassfactor
Pclassfactor <- factor(Pclassfactor) # factor function get vector and present us the vector levels
Pclassfactor
PclassData = factor(Pclassfactor, labels=c("Business", "Tourist", "Economy")) #set labels instead of levels numbers
PclassData
removecolumns$Pclass <- PclassData #Pclass Survived column
#View(removecolumns)

#Dealing with missing values
nrows <- nrow(removecolumns)
ncomplete <- sum(complete.cases(removecolumns))
ncomplete

# after that we get 0.8 , it means that by dropping all the rows with missing values, we are losing about 20% of data. So we cannot drop them. 
ncomplete/nrows


#convert function for setting text instead of int value
convertAge <- function(x){
  x <- ifelse(x<10, "Young", 
              ifelse(x<30, "Men",
                     ifelse(x>=30,"Old",)))
  x <- ifelse(is.na(x), "Men", x)
}

# We need sapplay to check the function for each value
tempAge <- sapply(removecolumns$Age, convertAge)
tempAge
removecolumns$Age <- tempAge
View(removecolumns)


#Package for algoritem naiveBayes
install.packages('e1071')
library(e1071)



#Package for confusion matrix
install.packages('SDMTools')
library(SDMTools)

# We need to revert the survived column to 0 & 1
conv_10 <- function(x){
  x<- ifelse(x=='Survived',1,0)
}

actual01 <- sapply(removecolumns$Survived, conv_10)


#Run the algoritem 
model <- naiveBayes(removecolumns[,-1], removecolumns$Survived)
model

# We need to do cunfusion matrix, for that we need to do all the action that we have done above on the test set
titanicTest<-read.csv('test.csv')
titanicTest
View(titanicTest)

removecolumnsTest<- titanicTest[,-c(1,3,8,9,10)] # remove columns 4,9 . we keeping column 1 for arrange it later
View(removecolumnsTest)

sibspfactor <- removecolumnsTest$SibSp #sibSp column into vector
sibspfactor
sibspfactor <- factor(sibspfactor) # factor function get vector and present us the vector levels
sibspfactor
data = factor(sibspfactor, labels=c("zero", "one", "two", "three", "four", "five", "eight")) #set labels instead of levels numbers
data


Parchfactor <- removecolumnsTest$Parch #Parch column into vector
Parchfactor
Parchfactor <- factor(Parchfactor) # factor function get vector and present us the vector levels
Parchfactor
data = factor(Parchfactor, labels=c("zero", "one", "two", "three", "four", "five", "six")) #set labels instead of levels numbers
data

data = factor(sibspfactor, labels=c("zero", "one", "two", "three", "four", "five", "eight")) #set labels instead of levels numbers
data
removecolumnsTest$SibSp <- data #repleace sibSp column


data = factor(Parchfactor, labels=c("zero", "one", "two", "three", "four", "five", "six")) #set labels instead of levels numbers
data
removecolumnsTest$Parch <- data #repleace Parch column
View(removecolumnsTest)


Pclassfactor <- removecolumnsTest$Pclass #Pclass column into vector
Pclassfactor
Pclassfactor <- factor(Pclassfactor) # factor function get vector and present us the vector levels
Pclassfactor
PclassData = factor(Pclassfactor, labels=c("Business", "Tourist", "Economy")) #set labels instead of levels numbers
PclassData
removecolumnsTest$Pclass <- PclassData #Pclass Survived column
#View(removecolumnsTest)

#Dealing with missing values
nrows <- nrow(removecolumnsTest)
ncomplete <- sum(complete.cases(removecolumnsTest))
ncomplete

# after that we get 0.8 , it means that by dropping all the rows with missing values, we are losing about 20% of data. So we cannot drop them. 
ncomplete/nrows


#convert function for setting text instead of int value
convertAge <- function(x){
  x <- ifelse(x<10, "Young", 
              ifelse(x<30, "Men",
                     ifelse(x>=30,"Old",)))
  x <- ifelse(is.na(x), "Men", x)
}

# We need sapplay to check the function for each value
tempAge <- sapply(removecolumnsTest$Age, convertAge)
tempAge
removecolumnsTest$Age <- tempAge
View(removecolumnsTest)

########################## End of preperation for test set #####################


# predict is getting the model (beys naivi) and the test set
prediction <- predict(model, removecolumnsTest)
prediction

View(removecolumnsTest)
confusion.matrix(actual01, prediction)


